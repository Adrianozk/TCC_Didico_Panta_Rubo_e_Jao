USE "WCM"

CREATE TABLE Veiculo (
    placa varchar(8) PRIMARY KEY,
    modelo varchar(max),
    cor varchar(max),
    marca varchar(max),
    motor varchar(max),
    ano varchar(4),
    FK_Usuario_cpf varchar(14)
);

CREATE TABLE Usuario (
    celular varchar(14),
	telefoneFixo varchar(14),
    cpf varchar(14) PRIMARY KEY,
    endereco varchar(max),
    cep varchar(9),
    cidade varchar(max),
    estado varchar(2),
    email varchar(max),
    senha VARCHAR(20),
    tipo_usuario varchar(20),
	ativo bit not null,
	nome varchar(max),
	bairro varchar(max) not null
);

CREATE TABLE Servico (
    servico_a_executar varchar(max),
    cod int identity(1,1) PRIMARY KEY,
    precoBase decimal(8,2),
    tempoMedio int
);

CREATE TABLE Ordem_de_servico (
    
	ordem int identity(1,1) PRIMARY KEY,
	tanque varchar(max),
    km_rodados varchar(max),
    
    data_entrada date,
    data_saida date,
    FK_Veiculo_placa varchar(8),

	cpf varchar(14) not null
);

CREATE TABLE pagamento(
	cod int identity(1,1) primary key,
	ordem int not null,
	total decimal(8,2) not null,
	tipo_de_pagamento varchar(max) not null,
	observacao varchar(max)
);

CREATE TABLE pertence (
    FK_Ordem_de_servico_ordem int,
    FK_Servico_cod int,
    preco decimal (5,2)
);
 
ALTER TABLE Veiculo ADD CONSTRAINT FK_Veiculo_1
    FOREIGN KEY (FK_Usuario_cpf)
    REFERENCES Usuario (cpf);
 
ALTER TABLE Ordem_de_servico ADD CONSTRAINT FK_Ordem_de_servico_1
    FOREIGN KEY (FK_Veiculo_placa)
    REFERENCES Veiculo (placa);

ALTER TABLE Ordem_de_servico ADD CONSTRAINT FK_Ordem_de_servico_2
    FOREIGN KEY (cpf)
    REFERENCES Usuario (cpf);

--ALTER TABLE Ordem_de_servico ADD CONSTRAINT FK_Ordem_de_servico_3
--    FOREIGN KEY (cod)
--    REFERENCES Servico (cod);

--ALTER TABLE Ordem_de_servico ADD CONSTRAINT FK_Ordem_de_servico_4
--    FOREIGN KEY (ordem)
--    REFERENCES Ordem_de_servico (ordem);
 
ALTER TABLE pertence ADD CONSTRAINT FK_pertence_0
    FOREIGN KEY (FK_Ordem_de_servico_ordem)
    REFERENCES Ordem_de_servico (ordem);
 
ALTER TABLE pertence ADD CONSTRAINT FK_pertence_1
    FOREIGN KEY (FK_Servico_cod)
    REFERENCES Servico (cod);


	--DROP TABLE Veiculo
	--DROP TABLE Usuario
	--DROP TABLE Servico
	--DROP TABLE Ordem_de_servico
	--DROP TABLE pertence
	--DROP TABLE pagamento

	SELECT * FROM Ordem_de_servico;
	INSERT INTO Veiculo

	INSERT INTO Usuario VALUES('555',null,'471.508.678-63','Rua Papa Santo, 56','13068-149','Campinas','SP','adrianoluisf@gmail.com','12345','Administrador',1,'Adriano Luis Fernandes','bairro real')
	INSERT INTO Usuario VALUES('555',null,'321.987.582-45','Rua Papa Santo, 56','13068-149','Campinas','SP','adrianoluisf@gmail.com','12345','Funcionário',1,'Adriano Luis Fernandes','bairro real')
	INSERT INTO Usuario VALUES('555',null,'123.456.978-65','Rua Papa Santo, 56','13068-149','Campinas','SP','adrianoluisf@gmail.com','12345','Cliente',1,'Adriano Luis Fernandes','bairro real')
	INSERT INTO Veiculo VALUES('1', '1','1', '1','1', '1','471.508.678-63')
	INSERT INTO Veiculo VALUES('2', '2','2', '2','2', '2','123.456.978-65')
	INSERT INTO  ordem_de_servico VALUES('1', '1', '10/10/1985', '12/12/2015','1','471.508.678-63')

	SELECT * FROM Usuario

	--DELETE FROM Veiculo
	--DELETE FROM Usuario
	--DELETE FROM Servico
	--DELETE FROM Ordem_de_servico
	--DELETE FROM telefone
	--DELETE FROM pertence

	SELECT * FROM Veiculo
	SELECT * FROM Usuario
	SELECT * FROM estado
	SELECT * FROM cidade
	SELECT * FROM Servico
	SELECT * FROM Ordem_de_servico
	SELECT * FROM pertence


	SELECT * FROM telefone
	SELECT * FROM pertence

	UPDATE Ordem_de_servico SET data_saida = '10/10/2000'
	UPDATE Usuario SET ativo = 1

	SELECT nom_cidade FROM estado e, cidade c WHERE sgl_estado = 'SP' AND c.cod_estado = e.cod_estado

	update usuario set FK_telefone_telefone_PK = '(19)4002-8922' where cpf = '456.618.708-09'

	SELECT placa FROM veiculo WHERE FK_Usuario_cpf = '123.456.978-65'


	CREATE TABLE acompanhamento (
  
    FK_ordem_de_servico_ordem int,
    situacao varchar (max)
);
 
 select * from acompanhamento

 INSERT INTO ordem_de_servico VALUES('500', '516841', '04/12/2018', '20/12/2018', 'KUY-6846','292.601.418-00')
 SELECT s.servico_a_executar, p.preco FROM pertence p, servico s, Ordem_de_servico o WHERE p.FK_Ordem_de_servico_ordem = 3 AND o.ordem = p.FK_Ordem_de_servico_ordem AND p.FK_Servico_cod = s.cod

SELECT ordem, tanque, km_rodados, data_entrada, data_saida, FK_Veiculo_placa, cpf FROM Ordem_de_servico o, pertence WHERE ordem LIKE '%1%' OR tanque LIKE '%1%' OR km_rodados LIKE '%1%' OR data_entrada LIKE '%1%'OR data_saida LIKE '%1%' OR FK_Veiculo_placa LIKE '%1%' OR cpf LIKE '%1%' GROUP BY ordem