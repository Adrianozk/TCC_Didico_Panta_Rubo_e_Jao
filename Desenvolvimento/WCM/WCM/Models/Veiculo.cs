﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WCM.Models
{
    public class Veiculo
    {
        //Declaração de variáveis
        private string placa, modelo, cor, marca, motor, ano, fkUsuario;

        //Métodos acessores e modificadores
        public string Ano
        {
            get
            {
                return ano;
            }

            set
            {
                ano = value;
            }
        }

        public string Cor
        {
            get
            {
                return cor;
            }

            set
            {
                cor = value;
            }
        }
        [Display(Name = "CPF do Cliente:")]
        public string FkUsuario
        {
            get
            {
                return fkUsuario;
            }

            set
            {
                fkUsuario = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Modelo
        {
            get
            {
                return modelo;
            }

            set
            {
                modelo = value;
            }
        }

        public string Motor
        {
            get
            {
                return motor;
            }

            set
            {
                motor = value;
            }
        }

        public string Placa
        {
            get
            {
                return placa;
            }

            set
            {
                placa = value;
            }
        }
        //Método para buscar placas de veículos de um usuário específico
        internal static List<string> buscaVeiculosDoUsuario(string cpf)
        {
            //Declarando lista de placas
            List<string> placas = new List<string>();
            try
            {
                //Abrindo conexão com o banco de dados
                Home.con.Open();

                //Query seleciona placa dos veículos do usuário
                MySqlCommand query = new MySqlCommand("SELECT placa FROM veiculo WHERE FK_Usuario_cpf = @cpf", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@cpf", cpf);
                //Declarando leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para a lista
                while (leitor.Read())
                {
                    placas.Add(leitor["placa"].ToString());
                }
                //Fechando conexão com o banco de dados
                Home.con.Close();

                return placas;
            }
            catch (Exception e)
            {
                return new List<string>();
                throw;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
        }

        internal string Inserir()
        {
            //Declarando string de resposta
            string res = "Inserido com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =//Query executa uma inserção de um Usuario no banco de dados
                    new MySqlCommand("INSERT INTO Veiculo VALUES(@placa, @modelo, @cor, @marca, @motor, "
                    + " @ano, @fkUsuario)", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@placa", Placa);
                query.Parameters.AddWithValue("@modelo", Modelo);
                query.Parameters.AddWithValue("@cor", Cor);
                query.Parameters.AddWithValue("@marca", Marca);
                query.Parameters.AddWithValue("@motor", Motor);
                query.Parameters.AddWithValue("@ano", Ano);
                query.Parameters.AddWithValue("@fkUsuario", FkUsuario);

                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }

        internal string Editar()
        {
            //Declarando string de resposta
            string res = "Inserido com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =//Query executa uma inserção de um Usuario no banco de dados
                    new MySqlCommand("UPDATE Veiculo SET modelo = @modelo, marca = @marca, "
                    + " cor = @cor, motor = @motor, ano = @ano WHERE placa = @placa)", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@placa", Placa);
                query.Parameters.AddWithValue("@modelo", Modelo);
                query.Parameters.AddWithValue("@cor", Cor);
                query.Parameters.AddWithValue("@marca", Marca);
                query.Parameters.AddWithValue("@motor", Motor);
                query.Parameters.AddWithValue("@ano", Ano);
                query.Parameters.AddWithValue("@fkUsuario", FkUsuario);
                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
        public Veiculo BuscaVeiculo()
        {
            //Criando um objeto de Usuario
            Veiculo v = new Veiculo();
            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona o Usuario quando tem a matricula especificada
                    new MySqlCommand("SELECT * FROM Veiculo WHERE placa = @placa", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@placa", placa);
                //Declarando o leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para o objeto
                while (leitor.Read())
                {
                    v.FkUsuario = leitor["FK_Usuario_cpf"].ToString();
                    v.placa = leitor["placa"].ToString();
                    v.modelo = leitor["modelo"].ToString();
                    v.marca = leitor["marca"].ToString();
                    v.cor = leitor["cor"].ToString();
                    v.ano = leitor["ano"].ToString();
                    v.motor = leitor["motor"].ToString();

                }
            }
            catch (Exception e)
            {
                //Como o método retorna um objeto
                //Quando acontece uma exceção é passado null para ele
                v = null;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna o objeto usuario buscado
            return v;
        }

        internal string Excluir()
        {
            //Declarando string de resposta
            string res = "Salvo com sucesso!";

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query executa um update na tabela Usuario colocando o ativo como 0
                                  //Quando o Usuario tem a matricula especificada
                    new MySqlCommand("UPDATE Veiculo SET " +
                    "ativo = 0" +
                    "WHERE placa = @placa", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@placa", Placa);
                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }


    }
}