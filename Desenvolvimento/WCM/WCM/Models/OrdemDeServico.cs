﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WCM.Models
{
    public class OrdemDeServico
    {
        //Declaração das variaveis
        private int ordem, cod;
        private string tanque;
        private string km_rodados;
        private DateTime data_saida, data_entrada;
        private string placa, cpf;

        //Propriedades das variaveis
        [DisplayName("ORDEM")]
        public int Ordem
        {
            get { return ordem; }
            set { ordem = value; }
        }

        [DisplayName("CPF")]
        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }

        [DisplayName("TANQUE")]
        public string Tanque
        {
            get { return tanque; }
            set { tanque = value; }
        }

        [DisplayName("CÓDIGO")]
        public int Cod
        {
            get { return cod; }
            set { cod = value; }
        }

        [DisplayName("KILÔMETROS RODADOS")]
        public string Km_rodados
        {
            get { return km_rodados; }
            set { km_rodados = value; }
        }

        [DisplayName("DATA DE ENTRADA")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Data_entrada
        {
            get { return data_entrada; }
            set { data_entrada = value; }
        }

        [DisplayName("DATA DE SAÍDA")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Data_saida
        {
            get { return data_saida; }
            set { data_saida = value; }
        }
        [DisplayName("PLACA")]
        public string Placa
        {
            get { return placa; }
            set { placa = value; }
        }

        //Cadastro de Ordem de Serviço
        internal string Cadastrar()
        {
            try
            {
                //Abrindo a conexão com o banco
                Home.con.Open();
                //Fazendo o comando de INSERT colocando os dados de médicos
                MySqlCommand query =
                    new MySqlCommand("INSERT INTO ordem_de_servico VALUES(null,@tanque, @km_rodados, @data_entrada, @data_saida, @placa,@cpf)", Home.con);

                query.Parameters.AddWithValue("@tanque", Tanque);
                query.Parameters.AddWithValue("@km_rodados", Km_rodados);
                query.Parameters.AddWithValue("@data_entrada", Data_entrada);
                query.Parameters.AddWithValue("@data_saida", DateTime.Now.AddDays(15));
                query.Parameters.AddWithValue("@placa", Placa);
                query.Parameters.AddWithValue("@cpf", Cpf);
                //query.Parameters.AddWithValue("@cod", Cod);

                query.ExecuteNonQuery();
            }
            //Caso se der erro
            catch (Exception e)
            {
                //Retorna a mensagem de erro
                return e.Message;
            }

            //Compara se a conexão está aberta, se estiber fecha
            if (Home.con.State == ConnectionState.Open)
                //Fecha conexão
                Home.con.Close();

            return "Inserido com sucesso!";
        }
        //Listar ordens de serviço
        public static List<OrdemDeServico> ListaServico()
        {
            //Declarando lista de objetos de Usuario
            List<OrdemDeServico> ordem = new List<OrdemDeServico>();

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona o Usuario quando estão ativos e tem um tipo de usuario ou outro
                  new MySqlCommand("SELECT * FROM Ordem_de_servico", Home.con);

                //Declarando leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados dos Usuarios
                while (leitor.Read())
                {
                    //Criando objeto de usuário e passando seus atributos do banco
                    OrdemDeServico serv = new OrdemDeServico();
                    serv.Ordem = int.Parse(leitor["Ordem"].ToString());
                    serv.Tanque = leitor["tanque"].ToString(); 
                    serv.Km_rodados = leitor["km_rodados"].ToString();
                    serv.Data_entrada = (DateTime)leitor["data_entrada"];
                    string teste = leitor["data_saida"].ToString();
                    //Nullable<DateTime> dateNull = null;
                    if (!teste.Equals(""))
                        serv.Data_saida = (DateTime)leitor["data_saida"];
                    else
                        serv.Data_saida = DateTime.Now;
                    serv.Placa = leitor["FK_Veiculo_placa"].ToString();
                    serv.Cpf = leitor["cpf"].ToString();

                    //serv.Cod = int.Parse(leitor["cod"].ToString());

                    //Adicionando o Usuario a lista
                    ordem.Add(serv);
                };
            }
            catch (Exception e)
            {
                //Como o retorno do método é uma lista de usuários
                //No caso de exceção é passado uma lista vazia
                ordem = new List<OrdemDeServico>();
            }
            //Abre a conexão com o banco de dados
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna a lsita de usuarios
            return ordem;
        }
        //Editar ordens de serviço
        internal string Editar()
        {
            //Declarando string de resposta
            string res = "Editado com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =
                    new MySqlCommand("UPDATE  ordem_de_servico SET tanque = @tanque, km_rodados = @km_rodados, data_entrada = @data_entrada, "
                    + " data_saida = @data_saida,cpf = @cpf WHERE ordem = @ordem", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@ordem", Ordem);
                query.Parameters.AddWithValue("@tanque", Tanque);
                query.Parameters.AddWithValue("@km_rodados", Km_rodados);
                query.Parameters.AddWithValue("@data_entrada", Data_entrada);
                query.Parameters.AddWithValue("@data_saida", Data_saida);
                query.Parameters.AddWithValue("@cpf", cpf);

                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
        //Buscar ordem de serviço específica
        public static OrdemDeServico BuscaOrdemDeServico(int ordem)
        {
            //Criando um objeto de serviço
            OrdemDeServico serv = new OrdemDeServico();
            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =
                    new MySqlCommand("SELECT * FROM ordem_de_servico WHERE ordem = @ordem", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@ordem", ordem);
                //Declarando o leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para o objeto
                while (leitor.Read())
                {
                    serv.Ordem = int.Parse(leitor["ordem"].ToString());
                    serv.Tanque = leitor["tanque"].ToString();
                    serv.Km_rodados = leitor["km_rodados"].ToString();
                    serv.Data_entrada = (DateTime)leitor["data_entrada"];
                    serv.Data_saida = (DateTime)leitor["data_saida"];
                    serv.Placa = leitor["FK_Veiculo_placa"].ToString();
                    serv.Cpf = leitor["cpf"].ToString();

                }
            }
            catch (Exception e)
            {
                //Como o método retorna um objeto
                //Quando acontece uma exceção é passado null para ele
                serv = null;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna o objeto usuario buscado
            return serv;
        }
        //Pesquisa a ordem de serviço por qualquer coluna do banco de dados
        public static List<OrdemDeServico> BuscaOrdemDeServico(string parametroQualquer)
        {
            List<OrdemDeServico> lista = new List<OrdemDeServico>();
            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =
                    new MySqlCommand(" SELECT * FROM Ordem_de_servico WHERE ordem LIKE @parametroQualquer OR tanque LIKE @parametroQualquer OR km_rodados LIKE @parametroQualquer OR data_entrada LIKE @parametroQualquer OR data_saida LIKE @parametroQualquer OR FK_Veiculo_placa LIKE @parametroQualquer OR cpf LIKE @parametroQualquer", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@parametroQualquer", "%" + parametroQualquer + "%");
                //Declarando o leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para o objeto
                while (leitor.Read())
                {
                    OrdemDeServico serv = new OrdemDeServico();
                    serv.Ordem = int.Parse(leitor["ordem"].ToString());
                    serv.Tanque = leitor["tanque"].ToString();
                    serv.Km_rodados = leitor["km_rodados"].ToString();
                    serv.Data_entrada = (DateTime)leitor["data_entrada"];
                    serv.Data_saida = (DateTime)leitor["data_saida"];
                    serv.Placa = leitor["FK_Veiculo_placa"].ToString();
                    serv.Cpf = leitor["cpf"].ToString();
                    lista.Add(serv);
                }
            }
            catch (Exception e)
            {
                //Como o método retorna um objeto
                //Quando acontece uma exceção é passado null para ele
                lista = null;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna o objeto usuario buscado
            return lista;
        }

        //public static OrdemDeServico BuscaOrdemDeServico(string ordem)
        //{
        //    //Criando um objeto de Usuario
        //    OrdemDeServico o = new OrdemDeServico();
        //    try
        //    {
        //        //Abre a conexão com o banco de dados
        //        Home.con.Open();
        //        MySqlCommand query =//Query seleciona o Usuario quando tem a matricula especificada
        //            new MySqlCommand("SELECT * FROM Ordem_de_servico WHERE ordem = @ordem", Home.con);
        //        //Passagem de parâmetro
        //        query.Parameters.AddWithValue("@ordem", ordem);
        //        //Declarando o leitor
        //        MySqlDataReader leitor = query.ExecuteReader();

        //        //Lendo os dados do banco e passando para o objeto
        //        while (leitor.Read())
        //        {
        //            o.Ordem = int.Parse(leitor["ordem"].ToString());
        //            o.Tanque = leitor["tanque"].ToString();
        //            o.Km_rodados = leitor["km_rodados"].ToString();
        //            o.Data_entrada = (DateTime) leitor["data_entrada"];
        //            o.Data_saida = (DateTime) leitor["data_saida"];
        //            o.Placa = leitor["FK_Veiculo_placa"].ToString();
        //            o.Cpf = leitor["cpf"].ToString();
        //            o.Cod = int.Parse(leitor["cod"].ToString());
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        //Como o método retorna um objeto
        //        //Quando acontece uma exceção é passado null para ele
        //        o = null;
        //    }
        //    //Testa se a conexão com o banco de dados está aberta e fecha
        //    if (Home.con.State == ConnectionState.Open)
        //        Home.con.Close();
        //    //Retorna o objeto usuario buscado
        //    return o;
        //}
        internal string Excluir()
        {
            //Declarando string de resposta
            string res = "Salvo com sucesso!";

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query executa um update na tabela Usuario colocando o ativo como 0
                                  //Quando o Usuario tem a matricula especificada
                    new MySqlCommand("DELETE FROM pertence WHERE FK_Ordem_de_servico_ordem = @ordem", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@ordem", Ordem);
                //Executa o comando SQL
                query.ExecuteNonQuery();
                MySqlCommand query2 =//Query executa um update na tabela Usuario colocando o ativo como 0
                                   //Quando o Usuario tem a matricula especificada
                    new MySqlCommand("DELETE FROM ordem_de_servico WHERE ordem = @ordem", Home.con);
                //Passagem de parâmetro
                query2.Parameters.AddWithValue("@ordem", Ordem);
                //Executa o comando SQL
                query2.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }

        internal string Buscar()
        {
            //Declarando string de resposta
            string res = "Salvo com sucesso!";

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query executa um update na tabela Usuario colocando o ativo como 0
                                  //Quando o Usuario tem a matricula especificada
                    new MySqlCommand("SELECT * FROM OrdemDeServico WHERE ordem = @ordem", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@ordem", Ordem);
                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }

        //Busca o código da ordem de serviço
        internal int BuscaCodOrdem()
        {
            //Declarando string de resposta
            int res = -1;

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();

                MySqlCommand query =//Seleciona o código da ordem de serviço quando tem o número mais alto com a placa especificada
                    new MySqlCommand("SELECT MAX(ordem) cod FROM Ordem_de_servico WHERE FK_Veiculo_placa = @placa", Home.con);

                //Passagem de parâmetro
                query.Parameters.AddWithValue("@placa", Placa);

                //Executa o comando SQL
                MySqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    res = int.Parse(leitor["cod"].ToString());
                }
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
    }
}