﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WCM.Models
{
    public class Servico
    {
        //Declatação de atributos
        private int cod;
        private string servico_a_executar;
        private double precoBase;
        private int tempoMedio;
        //Propriedades dos atributos
        [Display(Name = "SERVIÇO A EXECUTAR")]
        public string Servico_a_executar
        {
            get
            {
                return servico_a_executar;
            }

            set
            {
                servico_a_executar = value;
            }
        }
        [Display(Name = "PREÇO BASE")]
        public double PrecoBase
        {
            get
            {
                return precoBase;
            }

            set
            {
                precoBase = value;
            }
        }
        [Display(Name = "TEMPO MÉDIO")]
        public int TempoMedio
        {
            get
            {
                return tempoMedio;
            }

            set
            {
                tempoMedio = value;
            }
        }
        [Display(Name = "CÓDIGO")]
        public int Cod
        {
            get
            {
                return cod;
            }

            set
            {
                cod = value;
            }
        }

        internal string Inserir()
        {
            //Declarando string de resposta
            string res = "Inserido com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =//Query executa uma inserção de um serviço no banco de dados
                    new MySqlCommand("INSERT INTO Servico VALUES(@servico_a_executar,null, @precoBase, @tempoMedio)", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@servico_a_executar", Servico_a_executar);
                query.Parameters.AddWithValue("@precoBase", PrecoBase);
                query.Parameters.AddWithValue("@tempoMedio", TempoMedio);

                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }

        internal string Editar()
        {
            //Declarando string de resposta
            string res = "Inserido com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =//Query executa uma inserção de um Usuario no banco de dados
                    new MySqlCommand("UPDATE Servico SET servico_a_executar = @servico_a_executar, precoBase = @precoBase, tempoMedio= @tempoMedio WHERE cod = @cod", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@servico_a_executar", Servico_a_executar);
                query.Parameters.AddWithValue("@precoBase", PrecoBase);
                query.Parameters.AddWithValue("@tempoMedio", TempoMedio);
                query.Parameters.AddWithValue("@cod", Cod);


                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
        //Busca o código do serviçio pelo nome
        public static int BuscaCodServico(string servico)
        {
            int cod = -1;
            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona o Usuario quando tem a matricula especificada
                    new MySqlCommand("SELECT * FROM Servico WHERE servico_a_executar = @servico", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@servico", servico);
                //Declarando o leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para o objeto
                while (leitor.Read())
                {
                    cod = int.Parse(leitor["cod"].ToString());
                }
            }
            catch (Exception e)
            {
                return -1;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna o objeto usuario buscado
            return cod;
        }
        //Busca o serviço pelo código
        public static Models.Servico BuscaServico(int cod)
        {
            //Criando um objeto de serviço
            Servico v = new Servico();
            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =
                    new MySqlCommand("SELECT * FROM Servico WHERE cod = @cod", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@cod", cod);
                //Declarando o leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para o objeto
                while (leitor.Read())
                {
                    v.Servico_a_executar = leitor["servico_a_executar"].ToString();
                    v.PrecoBase = double.Parse(leitor["precoBase"].ToString());
                    v.TempoMedio = int.Parse(leitor["tempoMedio"].ToString());
                    v.Cod = int.Parse(leitor["cod"].ToString());


                }
            }
            catch (Exception e)
            {
                //Como o método retorna um objeto
                //Quando acontece uma exceção é passado null para ele
                v = null;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna o objeto usuario buscado
            return v;
        }
        //Lista os serviços com todos os dados
        public static List<Servico> ListaServico()
        {
            //Declarando lista de objetos de Usuario
            List<Servico> servico = new List<Servico>();

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona o Usuario quando estão ativos e tem um tipo de usuario ou outro
                  new MySqlCommand("SELECT * FROM Servico", Home.con);

                //Declarando leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados dos Usuarios
                while (leitor.Read())
                {
                    //Criando objeto de usuário e passando seus atributos do banco
                    Servico serv = new Servico();
                    serv.Servico_a_executar = leitor["servico_a_executar"].ToString();
                    serv.PrecoBase = double.Parse(leitor["precoBase"].ToString());
                    serv.TempoMedio = int.Parse(leitor["tempoMedio"].ToString());
                    serv.Cod = int.Parse(leitor["cod"].ToString());

                    //Adicionando o Usuario a lista
                    servico.Add(serv);
                }
            }
            catch (Exception e)
            {
                //Como o retorno do método é uma lista de usuários
                //No caso de exceção é passado uma lista vazia
                servico = new List<Servico>();
            }
            //Abre a conexão com o banco de dados
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna a lsita de usuarios
            return servico;
        }
        //Lista os nomes dos serviços
        public static List<string> ListaNomeServico()
        {
            //Declarando lista de objetos de Usuario
            List<string> servico = new List<string>();

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona o Usuario quando estão ativos e tem um tipo de usuario ou outro
                  new MySqlCommand("SELECT * FROM Servico", Home.con);

                //Declarando leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados dos Usuarios
                while (leitor.Read())
                {
                    //Adicionando o Usuario a lista
                    servico.Add(leitor["servico_a_executar"].ToString());
                }
            }
            catch (Exception e)
            {
                //Como o retorno do método é uma lista de usuários
                //No caso de exceção é passado uma lista vazia
                servico = new List<string>();
            }
            //Abre a conexão com o banco de dados
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna a lsita de usuarios
            return servico;
        }
        internal string Excluir()
        {
            //Declarando string de resposta
            string res = "Salvo com sucesso!";

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query executa um update na tabela Usuario colocando o ativo como 0
                                  //Quando o Usuario tem a matricula especificada
                    new MySqlCommand("DELETE FROM pertence WHERE FK_Servico_cod = @cod", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@cod", Cod);
                //Executa o comando SQL
                query.ExecuteNonQuery();

                MySqlCommand query2 =//Query executa um update na tabela Usuario colocando o ativo como 0
                                   //Quando o Usuario tem a matricula especificada
                    new MySqlCommand("DELETE FROM servico WHERE cod = @cod", Home.con);
                //Passagem de parâmetro
                query2.Parameters.AddWithValue("@cod", Cod);
                //Executa o comando SQL
                query2.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
    }
}