﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace WCM.Models
{
    public class Home
    {
        public static string conexaoString = ConfigurationManager.ConnectionStrings["STRING_CONNECTION"].ConnectionString;
        //conexão com o banco de dados
        public static MySqlConnection con = new MySqlConnection(conexaoString);
        //Declaração de atributos
        private string senha, cpf;
        public bool adm = false;
        public bool funci = false;
        private String email = "workshop_control@hotmail.com";
        private String senhaMail = "workshopcm2018";
        private String assunto, nome, telefone;
        private String texto;
        //Propriedades dos atributos
        public string Telefone
        {
            get
            {
                return telefone;
            }

            set
            {
                telefone = value;
            }
        }

        public string Nome
        {
            get
            {
                return nome;
            }

            set
            {
                nome = value;
            }
        }

        public string Cpf
        {
            get
            {
                return cpf;
            }

            set
            {
                cpf = value;
            }
        }
        public string Senha
        {
            get
            {
                return senha;
            }

            set
            {
                senha = value;
            }
        }
        public string Assunto
        {
            get { return assunto; }
            set { assunto = value; }
        }
        public string Texto
        {
            get { return texto; }
            set { texto = value; }
        }
        //@*°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠*@
        //o metodo que faz a conexão com o banco de dados para saber se o usuario existe ou não
        public string Logar()
        {
            Usuario usuario = new Usuario();
            try
            {
                //abre conexão
                con.Open();
                MySqlCommand query =
                    new MySqlCommand("SELECT * FROM Usuario WHERE cpf = @cpf AND senha = @senha", con);
                query.Parameters.AddWithValue("@cpf", cpf);
                query.Parameters.AddWithValue("@senha", senha);
                MySqlDataReader leitor = query.ExecuteReader();
                while (leitor.Read())
                {
                    usuario.TipoUsuario = leitor["tipo_usuario"].ToString();
                }
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }

            if (con.State == ConnectionState.Open)
                //fecha a conexão
                con.Close();
            return usuario.TipoUsuario;

        }
        //Enviar email para recado
        internal string Enviar()
        {

            try
            {
                //Configurando a mensagem
                MailMessage mail = new MailMessage();
                //Origem
                mail.From = new MailAddress(email);
                //Destinatário
                mail.To.Add("workshop_control@hotmail.com");
                //Assunto
                mail.Subject = assunto;
                //Corpo do e-mail
                mail.Body = texto;

                //Configurar o smtp
                SmtpClient smtpServer = new SmtpClient("smtp-mail.outlook.com");
                //configurou porta
                smtpServer.Port = 587;
                //Habilitou o TLS
                smtpServer.EnableSsl = true;
                //Configurou usuario e senha p/ logar
                smtpServer.Credentials = new System.Net.NetworkCredential(email, senhaMail);

                //Envia
                smtpServer.Send(mail);

            }
            catch (Exception e)
            {
                return "erro: " + e.Message.ToString();
                throw;
            }

            return "Enviado com sucesso!";
        }
    }
}