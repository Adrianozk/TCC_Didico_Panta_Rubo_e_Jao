﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WCM.Models
{
    public class Cidade
    {
        //Busca estados no banco de dados
        public static List<String> getEstados()
        {
            List<String> estados = new List<string>();
            try
            {
                Home.con.Open();

                MySqlCommand query = new MySqlCommand("SELECT * FROM estado", Home.con);
                MySqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        estados.Add(leitor["sgl_estado"].ToString());
                    }
                    Home.con.Close();
                    return estados;
                }
                else
                {
                    Home.con.Close();
                    return null;
                }
            }
            catch (Exception e)
            {
                Home.con.Close();
                return null;
                throw;
            }
        }
        //Busca cidades de um estado específico no banco de dados
        public static List<String> buscaCidades(string sglEstado)
        {
            List<String> cidades = new List<string>();
            try
            {
                Home.con.Open();

                MySqlCommand query = new MySqlCommand("SELECT nom_cidade FROM estado e, cidade c WHERE sgl_estado = @sglEstado AND c.cod_estado = e.cod_estado", Home.con);
                query.Parameters.AddWithValue("@sglEstado", sglEstado);
                MySqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        cidades.Add(leitor["nom_cidade"].ToString());
                    }
                    Home.con.Close();
                    return cidades;
                }
                else
                {
                    Home.con.Close();
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
                throw;
            }
        }
    }
}