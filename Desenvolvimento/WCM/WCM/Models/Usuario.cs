﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WCM.Models
{
    public class Usuario
    {
        //Declaração de variáveis
        private int ativo;
        private string cpf, endereco, cep, cidade, estado, email,
            senha, tipoUsuario, nome, telefoneFixo, celular, bairro;

        //Métodos acessores e modificadores
        [Display(Name = "TELEFONE FIXO")]
        public string Telefonefixo
        {
            get
            {
                return telefoneFixo;
            }

            set
            {
                telefoneFixo = value;
            }
        }

        [Display(Name = "CELULAR")]
        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        [Display(Name = "ATIVO")]
        public int Ativo
        {
            get
            {
                return ativo;
            }

            set
            {
                ativo = value;
            }
        }

        [Display(Name = "CPF")]
        public string Cpf
        {
            get
            {
                return cpf;
            }

            set
            {
                cpf = value;
            }
        }

        [Display(Name = "ENDEREÇO")]
        public string Endereco
        {
            get
            {
                return endereco;
            }

            set
            {
                endereco = value;
            }
        }

        [Display(Name = "CEP")]
        public string Cep
        {
            get
            {
                return cep;
            }

            set
            {
                cep = value;
            }
        }

        [DisplayName("CIDADE")]
        public string Cidade
        {
            get
            {
                return cidade;
            }

            set
            {
                cidade = value;
            }
        }

        [DisplayName("ESTADO")]
        public string Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        [Display(Name = "EMAIL")]
        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        [Display(Name = "SENHA")]
        public string Senha
        {
            get
            {
                return senha;
            }

            set
            {
                senha = value;
            }
        }
        [Display(Name = "TIPO DE USUÁRIO")]
        public string TipoUsuario
        {
            get
            {
                return tipoUsuario;
            }

            set
            {
                tipoUsuario = value;
            }
        }

        [Display(Name = "NOME")]
        public string Nome
        {
            get
            {
                return nome;
            }

            set
            {
                nome = value;
            }
        }


        [Display(Name = "BAIRRO")]
        public string Bairro
        {
            get
            {
                return bairro;
            }

            set
            {
                bairro = value;
            }
        }
        //Método para inserir Usuário no banco de dados
        internal string Inserir()
        {
            //Declarando string de resposta
            string res = "Inserido com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =//Query executa uma inserção de um Usuario no banco de dados
                    new MySqlCommand("INSERT INTO Usuario VALUES(@telefonefixo,@celular, @cpf, @endereco, @cep, @cidade, "
                    + " @estado, @email, @senha, @tipo_usuario, @ativo, @nome,@bairro)", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@telefonefixo", Telefonefixo);
                query.Parameters.AddWithValue("@celular", Celular);
                query.Parameters.AddWithValue("@cpf", Cpf);
                query.Parameters.AddWithValue("@endereco", Endereco);
                query.Parameters.AddWithValue("@cep", Cep);
                query.Parameters.AddWithValue("@cidade", Cidade);
                query.Parameters.AddWithValue("@estado", Estado);
                query.Parameters.AddWithValue("@email", Email);
                if (Senha == null)
                    query.Parameters.AddWithValue("@senha", DBNull.Value);
                else
                    query.Parameters.AddWithValue("@senha", Senha);
                query.Parameters.AddWithValue("@tipo_usuario", TipoUsuario);
                query.Parameters.AddWithValue("@ativo", 1);
                query.Parameters.AddWithValue("@nome", Nome);
                query.Parameters.AddWithValue("@bairro", Bairro);
                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
        //Método para fazer Update de Usuário no banco de dados
        internal string Editar()
        {
            //Declarando string de resposta
            string res = "Inserido com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =//Query executa um update de um Usuario no banco de dados
                    new MySqlCommand("UPDATE Usuario SET telefonefixo = @telefonefixo, celular = @celular, endereco = @endereco, cep = @cep, cidade = @cidade, "
                    + " estado = @estado, email = @email, senha = @senha, tipo_usuario = @tipo_usuario, nome = @nome, bairro=@bairro WHERE cpf = @cpf", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@telefonefixo", Telefonefixo);
                query.Parameters.AddWithValue("@celular", Celular);
                query.Parameters.AddWithValue("@cpf", Cpf);
                query.Parameters.AddWithValue("@endereco", Endereco);
                query.Parameters.AddWithValue("@cep", Cep);
                query.Parameters.AddWithValue("@cidade", Cidade);
                query.Parameters.AddWithValue("@estado", Estado);
                query.Parameters.AddWithValue("@email", Email);
                query.Parameters.AddWithValue("@senha", Senha);
                query.Parameters.AddWithValue("@tipo_usuario", TipoUsuario);
                query.Parameters.AddWithValue("@nome", Nome);
                query.Parameters.AddWithValue("@bairro", Bairro);
                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
        //Busca um usuário no banco de dados pelo cpf
        public static Usuario BuscaUsuarios(string cpf)
        {
            //Criando um objeto de Usuario 
            Usuario u = new Usuario();
            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona o Usuario quando tem o cpf especificado
                    new MySqlCommand("SELECT * FROM Usuario WHERE cpf = @cpf", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@cpf", cpf);
                //Declarando o leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para o objeto
                while (leitor.Read())
                {
                    u.Telefonefixo = leitor["telefonefixo"].ToString();
                    u.Celular = leitor["celular"].ToString();
                    u.Endereco = leitor["endereco"].ToString();
                    u.Cpf = leitor["cpf"].ToString();
                    u.Cep = leitor["cep"].ToString();
                    u.Cidade = leitor["cidade"].ToString();
                    u.Estado = leitor["estado"].ToString();
                    u.Email = leitor["email"].ToString();
                    u.Senha = leitor["senha"].ToString();
                    u.TipoUsuario = leitor["tipo_usuario"].ToString();
                    u.Nome = leitor["nome"].ToString();
                    u.Bairro = leitor["bairro"].ToString();
                }
            }
            catch (Exception e)
            {
                //Como o método retorna um objeto
                //Quando acontece uma exceção é passado null para ele
                u = null;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna o objeto usuario buscado
            return u;
        }
        //Método para listar usuários
        public static List<Usuario> ListaUsuario(string tipoUsuario)
        {
            //Declarando lista de objetos de Usuario
            List<Usuario> usuarios = new List<Usuario>();

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query;
                if (tipoUsuario == "")
                {
                    //Query seleciona o Usuario que estão ativos
                    query = new MySqlCommand("SELECT * FROM Usuario WHERE ativo = 1", Home.con);
                }
                else
                {
                    //Query seleciona o Usuario quando estão ativos e tem um tipo de usuario
                    query = new MySqlCommand("SELECT * FROM Usuario WHERE ativo = 1 AND" +
                    " (tipo_usuario = @tipoUsuario) ", Home.con);
                    //Delcaração de parâmetros
                    query.Parameters.AddWithValue("@tipoUsuario", tipoUsuario);
                }

                //Declarando leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados dos Usuarios
                while (leitor.Read())
                {
                    //Criando objeto de usuário e passando seus atributos do banco
                    Usuario usuario = new Usuario();
                    usuario.Telefonefixo = leitor["telefonefixo"].ToString();
                    usuario.Celular = leitor["celular"].ToString();
                    usuario.Cpf = leitor["cpf"].ToString();
                    usuario.Endereco = leitor["endereco"].ToString();
                    usuario.Cep = leitor["cep"].ToString();
                    usuario.Cidade = leitor["cidade"].ToString();
                    usuario.Estado = leitor["estado"].ToString();
                    usuario.Email = leitor["email"].ToString();
                    usuario.Senha = leitor["senha"].ToString();
                    usuario.TipoUsuario = leitor["tipo_usuario"].ToString();
                    usuario.Nome = leitor["nome"].ToString();
                    usuario.Bairro = leitor["bairro"].ToString();
                    //Adicionando o Usuario a lista
                    usuarios.Add(usuario);
                };
            }
            catch (Exception e)
            {
                //Como o retorno do método é uma lista de usuários
                //No caso de exceção é passado uma lista vazia
                usuarios = new List<Usuario>();
            }
            //Abre a conexão com o banco de dados
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna a lsita de usuarios
            return usuarios;
        }
        //Método para excluir usuário
        internal string Excluir()
        {
            //Declarando string de resposta
            string res = "Salvo com sucesso!";

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query executa um update na tabela Usuario colocando o ativo como 0
                                  //Quando o Usuario tem o cpf especificado
                    new MySqlCommand("UPDATE Usuario SET " +
                    "ativo = 0" +
                    "WHERE cpf = @cpf", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@cpf", Cpf);
                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
        //Método para fazer a validação do email
        public bool validaEmail()
        {
            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            if (rg.IsMatch(Email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Método para validar cpf
        public string validaCpf()
        {
            string strCpf = "";

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf = "", digito = "";
            strCpf = Cpf;
            strCpf = strCpf.Trim();
            strCpf = strCpf.Replace(".", "").Replace(",", "").Replace("-", "");
            if (strCpf.Length != 11)
            {
                return "CPF Inválido pois não tem o tamanho correto";
            }
            else
            {
                tempCpf = strCpf.Substring(0, 9);
                int soma = 0, resto = 0;
                for (int i = 0; i < 9; i++)
                {
                    soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
                }
                resto = soma % 11;
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;
                digito = resto.ToString();
                tempCpf = tempCpf + digito;
                soma = 0;
                for (int i = 0; i < 10; i++)
                    soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
                resto = soma % 11;
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;
                digito = digito + resto.ToString();
                //MessageBox.Show(digito + "\n" + strCpf.Substring(10));
                if (digito == strCpf.Substring(9))
                    return "CPF Válido";
                else
                {
                    return "CPF Inválido pois os digitos verificadores não batem";
                }
            }

        }
        public static Models.Usuario BuscaUsuario(string id)
        {
            Usuario p = new Usuario();
            try
            {
                Home.con.Open();
                MySqlCommand query =
                    new MySqlCommand("SELECT * FROM Usuario WHERE Cpf = @cpf", Home.con);
                query.Parameters.AddWithValue("@cpf", id);
                MySqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    //Criando objeto de usuário e passando seus atributos do banco
                    Usuario usuario = new Usuario();
                    usuario.Celular = leitor["celular"].ToString();
                    usuario.Telefonefixo = leitor["telefonefixo"].ToString();
                    usuario.Cpf = leitor["cpf"].ToString();
                    usuario.Endereco = leitor["endereco"].ToString();
                    usuario.Cep = leitor["cep"].ToString();
                    usuario.Cidade = leitor["cidade"].ToString();
                    usuario.Estado = leitor["estado"].ToString();
                    usuario.Email = leitor["email"].ToString();
                    usuario.Senha = leitor["senha"].ToString();
                    usuario.TipoUsuario = leitor["tipo_usuario"].ToString();
                    usuario.Nome = leitor["nome"].ToString();
                    usuario.Bairro = leitor["bairro"].ToString();

                }
            }
            catch (Exception e)
            {
                Home.con.Close();
                p = null;
            }

            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();

            return p;

        }
        //Método para listar somente os cpfs dos usuários
        public static List<string> ListaCpfs(string tipoUsuario)
        {
            //Declarando lista de objetos de Usuario
            List<string> cpfs = new List<string>();

            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona o Usuario quando estão ativos e tem um tipo de usuario específico
                  new MySqlCommand("SELECT * FROM Usuario WHERE ativo = 1 AND" +
                  " (tipo_usuario = @tipoUsuario) ", Home.con);
                //Delcaração de parâmetros
                query.Parameters.AddWithValue("@tipoUsuario", tipoUsuario);

                //Declarando leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os cpfs dos Usuarios
                while (leitor.Read())
                {
                    cpfs.Add(leitor["cpf"].ToString());
                }
            }
            catch (Exception e)
            {
                //Como o retorno do método é uma lista de string
                //No caso de exceção é passado uma lista vazia
                cpfs = new List<string>();
            }
            //Abre a conexão com o banco de dados
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna a lista de usuarios
            return cpfs;
        }
    }
}