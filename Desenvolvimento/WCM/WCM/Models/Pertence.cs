﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WCM.Models
{
    public class Pertence
    {
        //Declaração de atributos
        private int fkOrdem, fkCod;
        private double preco;
        private string servico;

        //Propriedades dos atributos
        public int FkCod
        {
            get
            {
                return fkCod;
            }

            set
            {
                fkCod = value;
            }
        }

        public int FkOrdem
        {
            get
            {
                return fkOrdem;
            }

            set
            {
                fkOrdem = value;
            }
        }

        public double Preco
        {
            get
            {
                return preco;
            }

            set
            {
                preco = value;
            }
        }

        public string Servico
        {
            get
            {
                return servico;
            }

            set
            {
                servico = value;
            }
        }

        internal string Inserir()
        {
            //Declarando string de resposta
            string res = "Inserido com sucesso!";

            try
            {
                Home.con.Open();
                MySqlCommand query =//Query executa uma inserção de um Usuario no banco de dados
                    new MySqlCommand("INSERT INTO pertence VALUES(@fkOrdem, @fkCod, @preco)", Home.con);
                //Passagem de parâmetros
                query.Parameters.AddWithValue("@fkOrdem", FkOrdem);
                query.Parameters.AddWithValue("@fkCod", FkCod);
                query.Parameters.AddWithValue("@preco", Preco);

                //Executa o comando SQL
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Passa a exceção para a string de retorno
                res = e.Message.ToString();
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == System.Data.ConnectionState.Open)
                Home.con.Close();
            //Retorna a resposta
            return res;
        }
        //Busca serviços que estão em uma OS
        public static List<string> BuscaServicosDaOrdem(int ordem)
        {
            //Criando uma lista de serviços
            List<string> listaServicos = new List<string>();
            try
            {
                //Abre a conexão com o banco de dados
                Home.con.Open();
                MySqlCommand query =//Query seleciona os serviços quando estão na Ordem de Serviços
                    new MySqlCommand("SELECT s.servico_a_executar, p.preco FROM pertence p, servico s, Ordem_de_servico o WHERE p.FK_Ordem_de_servico_ordem = @ordem AND o.ordem = p.FK_Ordem_de_servico_ordem AND p.FK_Servico_cod = s.cod", Home.con);
                //Passagem de parâmetro
                query.Parameters.AddWithValue("@ordem", ordem);
                //Declarando o leitor
                MySqlDataReader leitor = query.ExecuteReader();

                //Lendo os dados do banco e passando para o objeto
                while (leitor.Read())
                {
                    listaServicos.Add(leitor["servico_a_executar"].ToString() + " Preço: R$ " + leitor["preco"].ToString());
                }
            }
            catch (Exception e)
            {
                //Como o método retorna uma lista
                //Quando acontece uma exceção é passado uma lista vazia para ele
                return listaServicos;
            }
            //Testa se a conexão com o banco de dados está aberta e fecha
            if (Home.con.State == ConnectionState.Open)
                Home.con.Close();
            //Retorna o objeto usuario buscado
            return listaServicos;
        }
    }
}