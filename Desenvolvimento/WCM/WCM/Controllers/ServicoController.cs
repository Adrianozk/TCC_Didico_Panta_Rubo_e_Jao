﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCM.Models;

namespace WCM.Controllers
{
    public class ServicoController : Controller
    {
        // GET: Servico
        public ActionResult Servico()
        {
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View();
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//Cadastra serviço
        public ActionResult Servico(string servico_a_executar, double precoBase, int tempoMedio)
        {
            Servico m = new Servico();

            m.Servico_a_executar = servico_a_executar;
            m.PrecoBase = precoBase;
            m.TempoMedio = tempoMedio;
            TempData["Msg"] = m.Inserir();
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return RedirectToAction("Lista");
            else
                return RedirectToAction("Index", "Home");
        }
        //Lista serviços
        public ActionResult Lista()//retorno da view com os medicos 
        {
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View("Lista", Models.Servico.ListaServico());
            else
                return RedirectToAction("Index", "Home");
        }
        //Tela de editar
        public ActionResult Editar(int id)
        {
            Servico servico = Models.Servico.BuscaServico(id);
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View("Editar", servico);
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//update no serviço
        public ActionResult Editar(Servico servico)
        {
            TempData["Msg"] = servico.Editar();
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return RedirectToAction("Lista");
            else
                return RedirectToAction("Index", "Home");
        }
        //tela excluir
        public ActionResult Excluir(int id)
        {
            TempData["id"] = id;
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View(Models.Servico.BuscaServico(id));
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//excluir
        public ActionResult Excluir()
        {
            if (TempData["id"] != null)
            {
                Servico servico = new Servico();
                servico.Cod = int.Parse(TempData["id"].ToString());
                servico.Excluir();
            }
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return RedirectToAction("Lista");
            else
                return RedirectToAction("Index", "Home");
        }

    }
}