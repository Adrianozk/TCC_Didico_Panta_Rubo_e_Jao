﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCM.Models;

namespace WCM.Controllers
{
    public class UsuarioController : Controller
    {
        //Retorna cidades do estado que foi passado
        public JsonResult GetCidades(string UF)
        {
            return Json(Cidade.buscaCidades(UF), JsonRequestBehavior.AllowGet);
        }
        //Tela de cadastro
        public ActionResult Cadastrar()
        {
            ViewBag.listaCidades = new SelectList(Cidade.buscaCidades("AC"));
            ViewBag.listaEstados = new SelectList(Cidade.getEstados());
            if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                return View();
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//Cadastrar
        public ActionResult Cadastrar(Usuario usuario, string listaCidades, string listaEstados)
        {
            usuario.Cidade = listaCidades;
            usuario.Estado = listaEstados;
            //Testa todas as validações e cadastra o usuário caso esteja tudo ok
            if (usuario.validaEmail() && usuario.validaCpf().Equals("CPF Válido"))
            {
                if (usuario.TipoUsuario != "Cliente")
                {
                    if (usuario.Senha != null)
                    {
                        TempData["Msg"] = usuario.Inserir();
                        if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                            return RedirectToAction("Lista");
                        else
                            return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData["Msg"] = "É obrigatório preencher o campo senha ao cadastrar Funcionários ou Administradores";
                        ViewBag.listaCidades = new SelectList(Cidade.buscaCidades("AC"));
                        ViewBag.listaEstados = new SelectList(Cidade.getEstados());
                        if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                            return View("Cadastrar", usuario);
                        else
                            return RedirectToAction("Index", "Home");
                    }
                }
                else
                    TempData["Msg"] = usuario.Inserir();
                if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                    return RedirectToAction("Lista");
                else
                    return RedirectToAction("Index", "Home");
            }
            else if (usuario.validaCpf().Equals("CPF Válido"))
            {
                TempData["Msg"] = "Email inválido";
                ViewBag.listaCidades = new SelectList(Cidade.buscaCidades("AC"));
                ViewBag.listaEstados = new SelectList(Cidade.getEstados());
                if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                    return View("Cadastrar", usuario);
                else
                    return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["Msg"] = usuario.validaCpf();
                if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                    return RedirectToAction("Cadastrar");
                else
                    return RedirectToAction("Index", "Home");
            }
        }
        //Lista os usuários conforme o nível de acesso
        public ActionResult Lista()
        {
            string tipoUsuario = "";
            if (Session["Login"] != null)
            {
                if (Session["Login"].ToString().Equals("Funcionário"))
                    tipoUsuario = "Cliente";
                if (Session["Login"].ToString().Equals("Administrador"))
                    tipoUsuario = "";
                return View("Lista", Models.Usuario.ListaUsuario(tipoUsuario));
            }
            else
                return RedirectToAction("Index", "Home");
        }
        //Tela editar
        public ActionResult Editar(string id)
        {
            Usuario usuario = Usuario.BuscaUsuarios(id);
            ViewBag.listaCidades2 = new SelectList(Cidade.buscaCidades(usuario.Estado));
            ViewBag.listaEstados2 = new SelectList(Cidade.getEstados());
            if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                return View("Editar", usuario);
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//editar
        public ActionResult Editar(Usuario usuario, string listaCidades2, string listaEstados2)
        {
            usuario.Cidade = listaCidades2;
            usuario.Estado = listaEstados2;
            TempData["Msg"] = usuario.Editar();
            if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                return RedirectToAction("Lista");
            else
                return RedirectToAction("Index", "Home");
        }
        //tela de excluir
        public ActionResult Excluir(string id)
        {
            TempData["id"] = id;
            if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                return View(Usuario.BuscaUsuarios(id));
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//excluir
        public ActionResult Excluir()
        {
            if (TempData["id"] != null)
            {
                Usuario usuario = new Usuario();
                usuario.Cpf = TempData["id"].ToString();
                usuario.Excluir();
            }
            if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                return RedirectToAction("Lista");
            else
                return RedirectToAction("Index", "Home");
        }
        //Tela de detalhes do usuário
        public ActionResult Detalhes(string id)
        {
            //busca as placas dos veículos do usuário
            ViewBag.listaPlacas = new SelectList(Veiculo.buscaVeiculosDoUsuario(id));
            if (Session["Login"] != null && (Session["Login"].ToString() == "Funcionário" || Session["Login"].ToString() == "Administrador"))
                return View("Detalhe", Usuario.BuscaUsuarios(id));
            else
                return RedirectToAction("Index", "Home");
        }
        //Busca os dados de um veículo
        public JsonResult GetVeiculo(string id)
        {
            Veiculo veiculo = new Veiculo();
            veiculo.Placa = id;
            veiculo = veiculo.BuscaVeiculo();
            var resultado = new
            {
                ano = veiculo.Ano,
                cor = veiculo.Cor,
                marca = veiculo.Marca,
                modelo = veiculo.Modelo,
                motor = veiculo.Motor
            };
            return Json(resultado);
        }
    }
}