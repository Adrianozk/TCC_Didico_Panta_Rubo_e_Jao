﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCM.Models;

namespace WCM.Controllers
{
    public class OrdemDeServicoController : Controller
    {
        [HttpPost]//Cadastra
        public ActionResult Cadastrar(OrdemDeServico ordem_de_servico, string listaCpf, string listaPlacas, string tanque)
        {
            ordem_de_servico.Cpf = listaCpf;
            ordem_de_servico.Placa = listaPlacas;
            ordem_de_servico.Tanque = tanque;
            TempData["Msg"] = ordem_de_servico.Cadastrar();
            int id_ordem = ordem_de_servico.BuscaCodOrdem();
            if (id_ordem != -1)
            {
                return RedirectToAction("CadastrarPertence", new { id = id_ordem });
            }
            return RedirectToAction("Lista");
        }
        //Tela de listagem de OS
        public ActionResult Lista()
        {
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View("Lista", Models.OrdemDeServico.ListaServico());
            else
                return RedirectToAction("Index","Home");
        }
        //Tela de editar
        public ActionResult Editar(int id)
        {
            OrdemDeServico ordem_de_servico = Models.OrdemDeServico.BuscaOrdemDeServico(id);
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View("Editar", ordem_de_servico);
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//Efetua o update no banco de dados
        public ActionResult Editar(OrdemDeServico ordem_de_servico)
        {

            TempData["Msg"] = ordem_de_servico.Editar();
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return RedirectToAction("Lista");
            else
                return RedirectToAction("Index", "Home");
        }
        //Tela de Cadastrar
        public ActionResult Cadastrar()
        {
            ViewBag.listaCpf = new SelectList(Usuario.ListaCpfs("Cliente"));
            ViewBag.listaPlacas = new SelectList("123.456.978-65");
            ViewBag.listaServicos = new SelectList(Servico.ListaNomeServico());
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View();
            else
                return RedirectToAction("Index", "Home");
        }
        //Busca as placas dos veículos de um usuário
        public JsonResult GetPlacas(string CPF)
        {
            return Json(Veiculo.buscaVeiculosDoUsuario(CPF), JsonRequestBehavior.AllowGet);
        }
        //Tela para cadastrar serviços na OS
        public ActionResult CadastrarPertence(int id)
        {
            Pertence pertence = new Pertence();
            pertence.FkOrdem = id;
            Session["fkOrdem"] = id;
            ViewBag.listaServicosDaOrdem = Pertence.BuscaServicosDaOrdem(pertence.FkOrdem);
            ViewBag.listaServicos = new SelectList(Servico.ListaNomeServico());
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View("CadastrarPertence", pertence);
            else
                return RedirectToAction("Index", "Home");
        }
        //Cadastra o serviço na OS
        public JsonResult CadastrarPertenceJS(double preco)
        {
            Pertence pertence = new Pertence();
            pertence.Preco = preco;
            pertence.FkCod = int.Parse(Session["codServ"].ToString());
            pertence.FkOrdem = int.Parse(Session["fkOrdem"].ToString());
            TempData["Msg"] = pertence.Inserir();
            ViewBag.listaServicosDaOrdem = Pertence.BuscaServicosDaOrdem(pertence.FkOrdem);
            var resultado = new
            {
                listaServicos = ViewBag.listaServicosDaOrdem
            };
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        //retorna o código do serviço pelo nome
        public JsonResult BuscarCodServico(string servico)
        {
            int codServico = Servico.BuscaCodServico(servico);
            var resultado = new
            {
                cod = codServico
            };
            Session["codServ"] = codServico;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        //Mostra os detalhes da OS
        public ActionResult Detalhes(int id)
        {
            ViewBag.listaServicosDaOrdem = Pertence.BuscaServicosDaOrdem(id);
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View("Detalhes",Models.OrdemDeServico.BuscaOrdemDeServico(id));
            else
                return RedirectToAction("Index", "Home");
        }
        //tela de excluir
        public ActionResult Excluir(int id)
        {
            TempData["id"] = id;
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View(Models.OrdemDeServico.BuscaOrdemDeServico(id));
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//excluir
        public ActionResult Excluir()
        {
            if (TempData["id"] != null)
            {
                OrdemDeServico ordem = new OrdemDeServico();
                ordem.Ordem = int.Parse(TempData["id"].ToString());
                ordem.Excluir();
            }
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return RedirectToAction("Lista");
            else
                return RedirectToAction("Index", "Home");
        }
        //Tela de buscar
        public ActionResult Buscar()
        {
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View();
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//Buscar
        public ActionResult Buscar(string ordem)
        {
            List<OrdemDeServico> listaOrdem = Models.OrdemDeServico.BuscaOrdemDeServico(ordem);
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View("Lista", listaOrdem);
            else
                return RedirectToAction("Index", "Home");
        }
        //Retorna a ordem do banco
        public JsonResult BuscarOrdem(string ordem)
        {
            return Json(Models.OrdemDeServico.BuscaOrdemDeServico(ordem), JsonRequestBehavior.AllowGet);
        }
    }
}