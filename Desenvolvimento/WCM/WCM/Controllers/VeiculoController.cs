﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCM.Models;

namespace WCM.Controllers
{
    public class VeiculoController : Controller
    {
        //tela de cadastro
        public ActionResult Cadastrar(string id)
        {
            TempData["CPF"] = id;
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return View();
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]//cadastrar
        public ActionResult Cadastrar(Veiculo veiculo)
        {
            veiculo.FkUsuario = TempData["CPF"].ToString();
            TempData["Msg"] = veiculo.Inserir();
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
                return RedirectToAction("Lista", "Usuario");
            else
                return RedirectToAction("Index", "Home");
        }
        //Passando os atributos para editar os veiculos
        [HttpPost]
        public ActionResult Editar(string placa, string modelo, string cor, string marca, string motor, string fkUsuario)
        {
            Veiculo m = new Veiculo();
            m.Placa = placa;
            m.Modelo = modelo;
            m.Cor = cor;
            m.Marca = marca;
            m.Motor = motor;
            m.FkUsuario = fkUsuario;

            string res = m.Editar();
            TempData["Msg"] = res;
            if (Session["Login"] != null && Session["Login"].ToString() == "Funcionário")
            {
                if (res == "Salvo com sucesso!")
                    return RedirectToAction("Editar");
                else
                    return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }
    }
}