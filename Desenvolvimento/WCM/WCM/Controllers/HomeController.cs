﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCM.Models;

namespace WCM.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home

        // GET: Usuario
        //@*°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠*@
        //Retorna a tela de login
        public ActionResult Index()
        {
            return View();
        }
        //Metodo da tela de login
        [HttpPost]
        public ActionResult Index(string cpf, string senha)
        {
            Home u = new Home();
            u.Cpf = cpf;
            u.Senha = senha;
            string msg = u.Logar();
            ViewBag.Msg = msg;
            if (msg == "Funcionário" || msg == "Administrador")
            {
                TempData["Msg"] = "Usuário logado como " + msg;
                Session["Login"] = msg;
                return RedirectToAction("Index", "Home");
            }
            else
                TempData["Msg"] = "Este tipo de usuário não tem permissão";
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]//Envio de email
        public ActionResult Enviar(string assunto, string texto)
        {
            Models.Home v = new Models.Home();
            v.Assunto = assunto;
            v.Texto = texto;


            TempData["Msg"] = v.Enviar();
            return RedirectToAction("Index", "Home");
        }
        //@*°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠°🐠*@
        //Desloga do sistema
        public ActionResult Sair()
        {
            Session["Login"] = null;
            TempData["Msg"] = "Deslogado com sucesso";
            return RedirectToAction("Index", "Home");

        }
    }
}