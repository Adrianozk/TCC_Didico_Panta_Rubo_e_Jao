﻿$(document).ready(function () {
    $("#Cpf").mask("000.000.000-00");
    $("#Cep").mask("00000-000");
    $("#Telefone").mask("(00)00000-0000");
});

//Função para remover opções
function removeOptions(selectbox) {
    var i;
    for (i = selectbox.options.length - 1; i >= 0; i--) {
        selectbox.remove(i);
    }
}

////Popular cidades quando o estado é s
elecionado
function atualizaCidades() {
    $.post("GetCidades", { UF: document.getElementById('listaEstados').value }, function (data) {
        var select, i, option;
        select = document.getElementById('listaCidades');
        removeOptions(document.getElementById("listaCidades"));

        i = 0;
        data.forEach(function () {
            option = document.createElement("option");
            option.text = data[i];
            select.add(option);
            i++;
        });
    });
}

$(document).ready(function () {
    $('#Esconder').delay(20000).fadeOut();
});